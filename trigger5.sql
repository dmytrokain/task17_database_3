DELIMITER //
CREATE TRIGGER beforeUpdateMedicineZone
BEFORE UPDATE
ON medicine_zone FOR EACH ROW
BEGIN
	IF new.medicine_id != ALL ((SELECT id FROM medicine)) OR new.zone_id != ALL ((SELECT id FROM zone))
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Update Error!';
	END IF;
END //
DELIMITER ; 
