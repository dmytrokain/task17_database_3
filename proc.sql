DELIMITER //
CREATE PROCEDURE addEmployee(
	IN surname VARCHAR(30),
    IN name CHAR(30),
    IN midle_name VARCHAR(30),
    IN identity_number CHAR(10),
    IN passport CHAR(10),
    IN experience DECIMAL(10, 1),
    IN birthday DATE,
    IN post VARCHAR(15),
    IN pharmacy_id INT(11))
BEGIN
	IF post != ALL ((SELECT post FROM post)) OR pharmacy_id != ALL ((SELECT id FROM pharmacy))
    THEN SIGNAL SQLSTATE '45000' 
		SET MESSAGE_TEXT = 'Adding Error!';
    ELSE INSERT INTO employee (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id)
		VALUES (surname, name, midle_name, identity_number, passport, experience, birthday, post, pharmacy_id);
    END IF;
END//
DELIMITER ;
 
