DELIMITER //
CREATE TRIGGER beforeUpdateEmployee
BEFORE UPDATE
ON employee FOR EACH ROW
BEGIN
	IF new.post != ALL((SELECT post FROM post)) OR new.pharmacy_id != ALL((SELECT id FROM pharmacy))
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Update Error!';
    END IF;
END //
DELIMITER ; 
