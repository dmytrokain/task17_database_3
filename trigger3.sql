DELIMITER //
CREATE TRIGGER beforeDeleteMedicine
BEFORE DELETE
ON medicine FOR EACH ROW
BEGIN
	IF old.id = ANY ((SELECT medicine_id FROM medicine_zone)) or old.id = ANY ((SELECT medicine_id FROM pharmacy_medicine))
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Deletion Error!';
    END IF;
END //
DELIMITER  
